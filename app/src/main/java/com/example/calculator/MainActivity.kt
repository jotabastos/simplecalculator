package com.example.calculator

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.roundToLong


class MainActivity : AppCompatActivity() {
    // Declaration
    private var num1 = 0.0
    private var num2 = 0.0
    private var operation = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Clicks event
        btnOne.setOnClickListener { numberPressed("1") }
        btnTwo.setOnClickListener { numberPressed("2") }
        btnThree.setOnClickListener { numberPressed("3") }
        btnFour.setOnClickListener { numberPressed("4") }
        btnFive.setOnClickListener { numberPressed("5") }
        btnSix.setOnClickListener { numberPressed("6") }
        btnSeven.setOnClickListener { numberPressed("7") }
        btnEight.setOnClickListener { numberPressed("8") }
        btnNine.setOnClickListener { numberPressed("9") }
        btnZero.setOnClickListener { numberPressed("0") }
        btnPoint.setOnClickListener { numberPressed(".") }
        // Operations click event
        btnSum.setOnClickListener { operationPressed(SUM) }
        btnSubtraction.setOnClickListener { operationPressed(SUBTRACTION) }
        btnMultiply.setOnClickListener { operationPressed(MULTIPLY) }
        btnDivide.setOnClickListener { operationPressed(DIVIDE) }

        btnEqual.setOnClickListener { resolvePressed() }
        btnClear.setOnClickListener { resetAll() }


        // Default
        answer.text = "0"
        operation = VOID


    }

    // Constants
    companion object {
        const val SUM = 1
        const val SUBTRACTION = 2
        const val MULTIPLY = 3
        const val DIVIDE = 4
        const val VOID = 0
    }

    // Number click event
    private fun numberPressed(num: String){
        if(answer.text == "0" && num != ".") {
            answer.text = "$num"
        } else {
            answer.text = "${answer.text}$num"
        }

        if(operation == VOID){
            num1 = answer.text.toString().toDouble()
        } else {
            num2 = answer.text.toString().toDouble()
        }
    }
    // Equals button
    private fun resolvePressed(){

        val result = when(operation) {
            SUM -> num1 + num2
            SUBTRACTION -> num1 - num2
            MULTIPLY -> num1 * num2
            DIVIDE -> num1 / num2
            else -> null
        }
        // answer.text =  "$result" -> Remove .0
        answer.text =  "$result".replace(".0","")
    }

    private fun operationPressed(operation: Int){
        this.operation = operation
        num1 = answer.text.toString().toDouble()
        answer.text =  ""
    }
    // Clear
    private fun resetAll(){
        answer.text = "0"
        num1 = 0.0
        num2 = 0.0
    }



}